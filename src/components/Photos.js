import React, { Component } from 'react';
import './Photos.css';
class Photos extends Component {
  state = {
    albumId: null,
    emptyAlbum: false,
    picJson: []
  }
  async componentDidMount(){
    const resp = this.props.match.params;
    const respJson = await fetch(`https://jsonplaceholder.typicode.com/albums/${resp.id}/photos`);
    const picJson = await respJson.json();
    if (picJson.length === 0){
      this.setState({emptyAlbum: true})
    }else{
      this.setState({
        albumId: resp.id,
        picJson: picJson
      })
    }
    
  }
  render(){
    return <div>
    {(this.state.picJson.length > 0) ? <div><h2>Album: {this.state.albumId}</h2>
      <h3>Number of photos: {this.state.picJson.length}</h3>
      <div className="row">
      {this.state.picJson.map( (pic) => {
        return <Card title={pic.title} imgUrl="https://source.unsplash.com/random/300x300"/>
      })}</div>
      </div>
    : this.state.emptyAlbum ? <div className="alert alert-danger" role="alert">
        Album is empty
      </div>:<div className="alert alert-success" role="alert">
       Loading
      </div> }
    </div>
  }
}
const Card = (props) => {
  return <div className="col card-holder">
  <div className="card" style={{"width": "18rem"}}>
    <img className="card-img-top" src={props.imgUrl} alt="Card image cap" />
    <div className="card-body">
      <h5 className="card-title">{props.title}</h5>
      
      <a href="#" className="btn btn-primary">Go somewhere</a>
    </div>
  </div>
  </div>
}
export default Photos;