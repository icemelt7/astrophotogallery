import React, { Component } from 'react';
import { Link } from 'react-router-dom';
class Albums extends Component {
  state = {
    albumJson: []
  }
  async componentDidMount(){
    const resp = await fetch('https://jsonplaceholder.typicode.com/albums/');
    const json = await resp.json();
    this.setState({
      albumJson: json
    })
  }
  render(){
    return <div>
        {this.state.albumJson.map((album) => {
          return <Album id={album.id} title={album.title} />
        })}
    </div>
  }
}

const Album = (props) => {
  return <div>
    <Link to={`/${props.id}`}>{props.title}</Link>
  </div>
}

export default Albums;