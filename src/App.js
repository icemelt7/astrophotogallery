import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import NavBar from './components/NavBar';
import Page from './components/Page';
import Albums from './components/Albums';
import { Switch, Route } from 'react-router';
import Photos from './components/Photos';

class App extends Component {
  render() {
    return (
      <Page>
        <NavBar />
        <div className="container">
        <Switch>
          <Route exact path="/" component={Albums} />
          <Route path="/:id" component={Photos} />
        </Switch>
        </div>
      </Page>
    );
  }
}

export default App;
